# import pynvml

import subprocess




# Define the command to get the GPU information using nvidia-smi
command = "nvidia-smi --query-gpu=memory.used --format=csv,noheader"

# Execute the command inside the Docker container and get the output
output = subprocess.check_output(["/bin/bash", "-c", command]).decode().strip() # 

# Split the output by newlines to get the memory usage of each GPU
memory_usage_list = output.split("\n")

# Print the memory usage of each GPU
for i, memory_usage in enumerate(memory_usage_list):
    print(f"GPU {i}: Memory usage  using subprocess = {memory_usage} MB")



